package helloworld;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {
                "pretty",
                "html:target/cucumber-report.html",
                "json:target/cucumber-report.json"
        },
        features = {
//                "src/test/resources/helloworld/portschedule.feature",
                "src/test/resources/helloworld/MarskScheduleAPI.feature"
        }
)
public class RunCucumberTest {

}
