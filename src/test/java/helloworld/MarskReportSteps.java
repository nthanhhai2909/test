package helloworld;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

public class MarskReportSteps {

    List<Vessels> vessels;
    List<PortDetail> portDetails;
    private String vesselCode;

    @Given("Get init data")
    public void getInitData() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());

        HttpGet vessel = new HttpGet("https://api.maerskline.com/maeu/schedules/vessel/active");
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(vessel)) {
            String result = EntityUtils.toString(response.getEntity());
            JsonNode jsonNode = mapper.readValue(result, JsonNode.class);
            vessels = (List<Vessels>) mapper.readValue(jsonNode.get("vessels").toString(), List.class)
                    .stream()
                    .map(o -> {
                        Vessels item = new Vessels();
                        item.setCode(((LinkedHashMap<String, String>) o).get("code"));
                        item.setName(((LinkedHashMap<String, String>) o).get("name"));
                        return item;
                    }).collect(Collectors.toList());
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Then("Search vessel code {string}")
    public void searchVesselCode(String arg0) {
        for (Vessels item : vessels) {
            if (item.getName().equals(arg0)) {
                vesselCode = item.getCode();
                break;
            }
        }
    }

    @Then("search data by vessel code, from date {string} and to date {string}")
    public void searchDataByVesselCodeFromDateAndToDate(String fromDate, String toDate) {
        String query = "https://api.maerskline.com/maeu/schedules/vessel?vesselCode=" + vesselCode + "&fromDate=" + fromDate + "&toDate=" + toDate;
        HttpGet ports = new HttpGet(query);
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(ports)) {
            String result = EntityUtils.toString(response.getEntity());
            JsonNode jsonNode = mapper.readValue(result, JsonNode.class);
            portDetails = (List<PortDetail>) mapper.readValue(jsonNode.get("ports").toString(), List.class)
                    .stream()
                    .map(o -> {
                        PortDetail item = new PortDetail();
                        item.setPortGeoId(((LinkedHashMap<String, String>) o).get("portGeoId"));
                        item.setPort(((LinkedHashMap<String, String>) o).get("port"));
                        item.setTerminal(((LinkedHashMap<String, String>) o).get("terminal"));
                        item.setTerminalGeoId(((LinkedHashMap<String, String>) o).get("terminalGeoId"));
                        item.setVoyageArrival(((LinkedHashMap<String, String>) o).get("voyageArrival"));
                        item.setVoyageDeparture(((LinkedHashMap<String, String>) o).get("voyageDeparture"));
                        item.setArrival(((LinkedHashMap<String, String>) o).get("arrival"));
                        item.setDeparture(((LinkedHashMap<String, String>) o).get("departure"));
                        item.setServiceArr(((LinkedHashMap<String, String>) o).get("serviceArr"));
                        item.setServiceDep(((LinkedHashMap<String, String>) o).get("serviceDep"));
                        return item;
                    }).collect(Collectors.toList());
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Then("send report email to user {string}")
    public void sendReportEmailToUser(String email) {
        XSSFWorkbook workbook = new XSSFWorkbook();
        int rownum = 0;
        XSSFSheet sheet = workbook.createSheet("Data");

        Row header = sheet.createRow(rownum++);
        header.createCell(0).setCellValue("Port Geo ID");
        header.createCell(1).setCellValue("Port");
        header.createCell(2).setCellValue("Terminal");
        header.createCell(3).setCellValue("Terminal Geo Id");
        header.createCell(4).setCellValue("Voyage Arrival");
        header.createCell(5).setCellValue("Voyage Departure");
        header.createCell(6).setCellValue("Arrival");
        header.createCell(7).setCellValue("Departure");
        header.createCell(8).setCellValue("Service Arr");
        header.createCell(9).setCellValue("Service Dep");

        for (PortDetail item : portDetails) {
            Row row = sheet.createRow(rownum++);
            row.createCell(0).setCellValue(item.getPortGeoId());

            row.createCell(1).setCellValue(item.getPort());

            row.createCell(2).setCellValue(item.getTerminal());

            row.createCell(3).setCellValue(item.getTerminalGeoId());

            row.createCell(4).setCellValue(item.getVoyageArrival());

            row.createCell(5).setCellValue(item.getVoyageDeparture());

            row.createCell(6).setCellValue(item.getArrival());

            row.createCell(7).setCellValue(item.getDeparture());

            row.createCell(8).setCellValue(item.getServiceArr());

            row.createCell(9).setCellValue(item.getServiceDep());

        }
        try {
            File file = new File("report.xlsx");
            FileOutputStream out = new FileOutputStream(file);
            workbook.write(out);
            out.close();

            final String fromEmail = "oceanscanner@cyberlogitec.com";
            // Mat khai email cua ban
            final String password = "cyber6350!";
            // dia chi email nguoi nhan
            final String subject = "Sealand report";
            String today = LocalDate.now().toString();
            final String body = "Dear Admin, \n" + "Here is the port schedule report";
            Properties props = new Properties();
            props.put("mail.smtp.host", "gwx.bizmeka.com");
            props.put("mail.smtp.port", "25");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(fromEmail, password);
                }
            });
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(fromEmail));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email, false));
            message.setSubject(subject);
            // Phan 1 gom doan tin nhan
            BodyPart messageBodyPart1 = new MimeBodyPart();
            messageBodyPart1.setText(body);
            // phan 2 chua tap tin txt
            MimeBodyPart messageBodyPart2 = new MimeBodyPart();
            // Duong dan den file cua ban
            String filePath = "report.xlsx";
            DataSource source1 = new FileDataSource(filePath);
            messageBodyPart2.setDataHandler(new DataHandler(source1));
            messageBodyPart2.setFileName(filePath);
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart1);
            multipart.addBodyPart(messageBodyPart2);
            message.setContent(multipart);
            Transport.send(message);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static final class Vessels {
        private String code;
        private String name;

        public Vessels() {
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static final class PortDetail {
        private String portGeoId;
        private String port;
        private String terminal;
        private String terminalGeoId;
        private String voyageArrival;
        private String voyageDeparture;
        private String arrival;
        private String departure;
        private String serviceArr;
        private String serviceDep;

        public PortDetail() {
        }

        public String getPortGeoId() {
            return portGeoId;
        }

        public void setPortGeoId(String portGeoId) {
            this.portGeoId = portGeoId;
        }

        public String getPort() {
            return port;
        }

        public void setPort(String port) {
            this.port = port;
        }

        public String getTerminal() {
            return terminal;
        }

        public void setTerminal(String terminal) {
            this.terminal = terminal;
        }

        public String getTerminalGeoId() {
            return terminalGeoId;
        }

        public void setTerminalGeoId(String terminalGeoId) {
            this.terminalGeoId = terminalGeoId;
        }

        public String getVoyageArrival() {
            return voyageArrival;
        }

        public void setVoyageArrival(String voyageArrival) {
            this.voyageArrival = voyageArrival;
        }

        public String getVoyageDeparture() {
            return voyageDeparture;
        }

        public void setVoyageDeparture(String voyageDeparture) {
            this.voyageDeparture = voyageDeparture;
        }

        public String getArrival() {
            return arrival;
        }

        public void setArrival(String arrival) {
            this.arrival = arrival;
        }

        public String getDeparture() {
            return departure;
        }

        public void setDeparture(String departure) {
            this.departure = departure;
        }

        public String getServiceArr() {
            return serviceArr;
        }

        public void setServiceArr(String serviceArr) {
            this.serviceArr = serviceArr;
        }

        public String getServiceDep() {
            return serviceDep;
        }

        public void setServiceDep(String serviceDep) {
            this.serviceDep = serviceDep;
        }
    }

}

