package helloworld;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.*;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class PostScheduleSteps {

    private final WebDriver driver;
    List<Item> items = new ArrayList<>();

    public PostScheduleSteps() {
        System.setProperty("webdriver.chrome.driver", "libs/chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Given("I goto one site")
    public void gotoOneSite() {
        driver.get("https://ecomm.one-line.com/ecom/CUP_HOM_3006.do?sessLocale=en");
        driver.manage().window().maximize();
    }

    @When("input port {string}")
    public void inputPort(String arg0) throws InterruptedException {
        uiLookup("//*[@id=\"portNm\"]").sendKeys(arg0);
        Thread.sleep(1000);
        uiLookup("//*[@id=\"portNm\"]").sendKeys(Keys.ARROW_DOWN);
        uiLookup("//*[@id=\"portNm\"]").sendKeys(Keys.ENTER);
    }

    @Then("click search button")
    public void clickSearchButton() {
        uiLookup("//*[@id=\"btnSearch\"]").click();
    }

    private WebElement uiLookup(String xpath) {
        return new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
    }

    @Then("analyze result")
    public void analyzeResult() throws InterruptedException, JsonProcessingException, UnsupportedEncodingException {
        Thread.sleep(1000);
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        int total = Integer.valueOf(uiLookup("//*[@id=\"totCnt\"]").getText());
        for (int i = 1; i <= total; i++) {
            String vessel = uiLookup("//*[@id=\"skdTb\"]/tbody/tr[" + i + "]/td[" + 2 + "]").getText();
            String terminal = uiLookup("//*[@id=\"skdTb\"]/tbody/tr[" + i + "]/td[" + 3 + "]").getText();
            String serviceLane = uiLookup("//*[@id=\"skdTb\"]/tbody/tr[" + i + "]/td[" + 4 + "]").getText();
            String arrivalTime = uiLookup("//*[@id=\"skdTb\"]/tbody/tr[" + i + "]/td[" + 5 + "]").getText();
            String berthingTime = uiLookup("//*[@id=\"skdTb\"]/tbody/tr[" + i + "]/td[" + 6 + "]").getText();
            String departure = uiLookup("//*[@id=\"skdTb\"]/tbody/tr[" + i + "]/td[" + 7 + "]").getText();
            String portCutOffTime = uiLookup("//*[@id=\"skdTb\"]/tbody/tr[" + i + "]/td[" + 8 + "]").getText();
            Item item = new Item();
            item.setVessel(vessel);
            item.setTerminal(terminal);
            item.setServiceLane(serviceLane);
            item.setArrivalTime(LocalDateTime.parse(arrivalTime, formatter2));
            item.setBerthingTime(LocalDateTime.parse(berthingTime, formatter2));
            item.setDeparture(LocalDateTime.parse(departure, formatter2));
            item.setPortCutOffTime(LocalDateTime.parse(portCutOffTime, formatter1));
            items.add(item);
        }

        HttpPost post = new HttpPost("http://10.0.0.95:8083/studio/one");
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        List<NameValuePair> urlParameters = new ArrayList<>();
        urlParameters.add(new BasicNameValuePair("items", mapper.writeValueAsString(items)));
        post.setEntity(new UrlEncodedFormEntity(urlParameters));
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(post)) {
            String result = EntityUtils.toString(response.getEntity());
            System.out.println(result);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Then("quit browser")
    public void quitBrowser() {
        driver.quit();
    }

    @Then("Input Period")
    public void inputPeriod() throws InterruptedException {
        Thread.sleep(1000);
        uiLookup("//*[@id=\"frmDt\"]").click();
        String today = LocalDate.now().toString();
        uiLookup("//*[@id=\"frmDt\"]").sendKeys(today);
        uiLookup("//*[@id=\"frmDt\"]").sendKeys(Keys.ENTER);
        Thread.sleep(1000);
        uiLookup("//*[@id=\"toDt\"]").click();
        uiLookup("//*[@id=\"toDt\"]").sendKeys(today);
        uiLookup("//*[@id=\"frmDt\"]").sendKeys(Keys.ENTER);
    }

    @Then("send email to {string}")
    public void sendEmailTo(String arg0) {
        XSSFWorkbook workbook = new XSSFWorkbook();
        int rownum = 0;
        XSSFSheet sheet = workbook.createSheet("Data");

        Row header = sheet.createRow(rownum++);
        header.createCell(0).setCellValue("Vessel");
        header.createCell(1).setCellValue("Terminal");
        header.createCell(2).setCellValue("Service Lane");
        header.createCell(3).setCellValue("Arrival Time");
        header.createCell(4).setCellValue("Berthing Time");
        header.createCell(5).setCellValue("Departure Time");
        header.createCell(6).setCellValue("Port Cut Off Time");

        for (Item item : items) {
            Row row = sheet.createRow(rownum++);
            row.createCell(0).setCellValue(item.getVessel());

            row.createCell(1).setCellValue(item.getTerminal());

            row.createCell(2).setCellValue(item.getServiceLane());

            row.createCell(3).setCellValue(item.getArrivalTime());

            row.createCell(4).setCellValue(item.getBerthingTime());

            row.createCell(5).setCellValue(item.getDeparture());

            row.createCell(6).setCellValue(item.getPortCutOffTime());
        }

        try {
            // ghi dữ liệu xuống file
            File file = new File("report.xlsx");
            FileOutputStream out = new FileOutputStream(file);
            workbook.write(out);
            out.close();

            final String fromEmail = "oceanscanner@cyberlogitec.com";
            // Mat khai email cua ban
            final String password = "cyber6350!";
            // dia chi email nguoi nhan
            final String subject = "One report";
            String today = LocalDate.now().toString();
            final String body = "Dear Admin, \n" + "Here is the port schedule report";
            Properties props = new Properties();
            props.put("mail.smtp.host", "gwx.bizmeka.com");
            props.put("mail.smtp.port", "25");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(fromEmail, password);
                }
            });
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(fromEmail));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(arg0, false));
            message.setSubject(subject);
            // Phan 1 gom doan tin nhan
            BodyPart messageBodyPart1 = new MimeBodyPart();
            messageBodyPart1.setText(body);
            // phan 2 chua tap tin txt
            MimeBodyPart messageBodyPart2 = new MimeBodyPart();
            // Duong dan den file cua ban
            String filePath = "report.xlsx";
            DataSource source1 = new FileDataSource(filePath);
            messageBodyPart2.setDataHandler(new DataHandler(source1));
            messageBodyPart2.setFileName(filePath);
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart1);
            multipart.addBodyPart(messageBodyPart2);
            message.setContent(multipart);
            Transport.send(message);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

class Item implements Serializable {
    private String vessel;
    private String terminal;
    private String serviceLane;
    private LocalDateTime arrivalTime;
    private LocalDateTime berthingTime;
    private LocalDateTime departure;
    private LocalDateTime portCutOffTime;

    public Item() {
    }

    public String getVessel() {
        return vessel;
    }

    public void setVessel(String vessel) {
        this.vessel = vessel;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getServiceLane() {
        return serviceLane;
    }

    public void setServiceLane(String serviceLane) {
        this.serviceLane = serviceLane;
    }

    public LocalDateTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(LocalDateTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public LocalDateTime getBerthingTime() {
        return berthingTime;
    }

    public void setBerthingTime(LocalDateTime berthingTime) {
        this.berthingTime = berthingTime;
    }

    public LocalDateTime getDeparture() {
        return departure;
    }

    public void setDeparture(LocalDateTime departure) {
        this.departure = departure;
    }

    public LocalDateTime getPortCutOffTime() {
        return portCutOffTime;
    }

    public void setPortCutOffTime(LocalDateTime portCutOffTime) {
        this.portCutOffTime = portCutOffTime;
    }
}
