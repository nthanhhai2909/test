Feature: Marsk Report API

  Scenario: Marsk Report Schedule API
    Given Get init data
    Then Search vessel code "FILOTIMO"
    Then search data by vessel code, from date "2021-03-20" and to date "2021-05-01"
    Then send report email to user "hai.nguyenthanh@cyberlogitec.com"